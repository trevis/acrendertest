﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ACRenderTestPlugin {
    public class Hooker {
        internal IntPtr Entrypoint;
        internal Delegate Del;
        internal int call;
        internal bool Hooked = false;

        public Hooker(int entrypoint, int call_location) {
            Entrypoint = (IntPtr)entrypoint;
            call = call_location;
        }
        public void Setup(Delegate del) {
            if (!Hooked) {
                if (VideoPatch.ReadCall(call) != (int)Entrypoint) {
                    //Core.WriteToDebugLog($"Failed to detour 0x{call:X8}. expected 0x{(int)Entrypoint:X8}, received 0x{P.ReadCall(call):X8}");
                    return;
                }
                Hooked = true;
                Del = del;
                if (!VideoPatch.PatchCall(call, Marshal.GetFunctionPointerForDelegate(Del))) {
                    Del = null;
                    Hooked = false;
                }
                else {
                    VideoPatch.hookers.Add(this);
                    //Core.WriteToDebugLog($"Detouring {(int)Entrypoint:X8}");
                }
            }
        }
        public void Remove() {
            if (Hooked) {
                if (VideoPatch.PatchCall(call, Entrypoint)) {
                    Del = null;
                    Hooked = false;
                    VideoPatch.hookers.Remove(this);
                    //Core.WriteToDebugLog($"Un-detouring {(int)Entrypoint:X8}");
                }
            }
        }
    }

    public unsafe class VideoPatch : IDisposable {
        internal static bool _enabled = false;
        internal static readonly UIntPtr intsize = new UIntPtr(sizeof(int));
        internal static int* GlobalEventHandler = (int*)0x00838374;

        [DllImport("kernel32.dll")] internal static extern bool VirtualProtectEx(IntPtr hProcess, IntPtr lpAddress, UIntPtr dwSize, int flNewProtect, out int lpflOldProtect);
        [DllImport("user32.dll")] internal static extern int GetForegroundWindow();

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate void def_Device__Deactivate(); // void __cdecl Device::Deactivate() 
        internal static void Call_Device__Deactivate() => ((def_Device__Deactivate)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00439320, typeof(def_Device__Deactivate)))();

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_SmartBox__SetNormalMode(int SmartBox); //int __thiscall SmartBox::SetNormalMode(SmartBox*this)
        internal static void Call_SmartBox__SetNormalMode(int SmartBox) => ((def_SmartBox__SetNormalMode)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00453160, typeof(def_SmartBox__SetNormalMode)))(SmartBox);

        internal static void Call_SmartBox__update_viewer(int SmartBox) => ((def_SmartBox__update_viewer)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00453D80, typeof(def_SmartBox__update_viewer)))(SmartBox);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_SmartBox__update_viewer(int SmartBox);// void __thiscall SmartBox::update_viewer(SmartBox *this)

        internal static void Call_SmartBox__Draw(int SmartBox) => ((def_SmartBox__Draw)Marshal.GetDelegateForFunctionPointer(SmartBox__Draw.Entrypoint, typeof(def_SmartBox__Draw)))(SmartBox);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_SmartBox__Draw(int SmartBox); // ???? not in source
        internal static Hooker SmartBox__Draw = new Hooker(0x00455610, 0x00412006);

        /// <summary>
        /// Disables rendering the 3d portion of the game client.
        /// </summary>
        public static bool Enabled {
            get => _enabled;
            set {
                if (value == _enabled) return;
                _enabled = value;
                if (value) {
                    if (GetForegroundWindow() != (int)CoreManager.Current.Decal.Hwnd) Call_Device__Deactivate();
                    SmartBox__Draw.Setup(new def_SmartBox__Draw(Hook_SmartBox__Draw));
                }
                else {
                    SmartBox__Draw.Remove();
                }
            }
        }

        public VideoPatch() {
            Enabled = true;
        }

        #region ResolveHandler(int handlerID)
        internal static int ResolveHandler(int handlerID) {
            try {
                int m_handlers = *(int*)((*(int*)(*GlobalEventHandler + 4)) + 8 + ((handlerID % 0x17) << 2));
                while (*(int*)m_handlers != handlerID) m_handlers = *(int*)(m_handlers + 4);
                return *(int*)*(int*)(*(int*)(m_handlers + 8) + 4);
            }
            catch { return 0; }
        }
        #endregion
        #region Write(IntPtr address, <int,float,byte> newValue)
        internal static void Write(IntPtr address, int newValue) {
            try {
                VirtualProtectEx(Process.GetCurrentProcess().Handle, address, intsize, 0x40, out int b);
                *(int*)address = newValue;
                VirtualProtectEx(Process.GetCurrentProcess().Handle, address, intsize, b, out b);
            }
            catch { }
        }

        #endregion

        #region PatchCall(int callLocation, IntPtr newPointer)
        internal static bool PatchCall(int callLocation, IntPtr newPointer) {
            if (((*(byte*)callLocation) & 0xFE) != 0xE8)
                return false;
            int previousOffset = *(int*)(callLocation + 1);
            int previousPointer = previousOffset + (callLocation + 5);
            int newOffset = (int)newPointer - (callLocation + 5);
            Write((IntPtr)(callLocation + 1), newOffset);
            return true;
        }
        #endregion
        #region ReadCall(int callLocation)
        internal static int ReadCall(int callLocation) {
            if (((*(byte*)callLocation) & 0xFE) != 0xE8)
                return 0;
            int previousOffset = *(int*)(callLocation + 1);
            int previousPointer = previousOffset + (callLocation + 5);
            return previousPointer;
        }
        #endregion


        internal static List<Hooker> hookers = new List<Hooker>();

        internal static void Hook_SmartBox__Draw(int SmartBox) {
            Call_SmartBox__SetNormalMode(SmartBox);
            Call_SmartBox__update_viewer(SmartBox);
        }

        public void Dispose() {
            Enabled = false;
            for (int i = hookers.Count - 1; i > -1; i--)
                hookers[i].Remove();
        }
    }
}
