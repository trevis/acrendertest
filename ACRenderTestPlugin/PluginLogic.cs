﻿using ACE.DatLoader;
using ACRenderTestPlugin.Views;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Microsoft.DirectX.Direct3D;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Serialization;
using VirindiViewService;
using VirindiViewService.Controls;
using VirindiViewService.XMLParsers;

namespace ACRenderTestPlugin {
    /// <summary>
    /// This is where all your plugin logic should go.  Public fields are automatically serialized and deserialized
    /// between plugin sessions in this class.  Check out the main Plugin class to see how the serialization works.
    /// </summary> 
    public class PluginLogic {
        #region Serialized Settings
        public bool ShouldEnableVideoPatch { get; set; }
        public bool ShouldEnableRenderer { get; set; }
        public bool ShouldUpdateViewTransform { get; set; }
        public bool ShouldRenderCurrentLandblock { get; set; }
        #endregion //Serialized Settings

        // ignore a specific public field
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public static string PluginAssemblyDirectory;

        // references to our view stuff is kept private so it is not serialized. 
        internal static HudView view;
        private ViewProperties properties;
        private ControlGroup controls;

        private HudCheckBox EnableRendererCheckbox;
        private HudCheckBox EnableVideoPatchCheckbox;
        private HudFixedLayout D3DBooleanOptionsLayout;
        private HudFixedLayout D3DEnumOptionsLayout;
        private HudFixedLayout D3DEnum2OptionsLayout;
        private HudFixedLayout D3DIntOptionsLayout;
        private HudFixedLayout D3DFloatOptionsLayout;
        private HudFixedLayout D3DColorOptionsLayout;
        private HudTabView Notebook;
        private HudTabView D3DNotebook;
        private HudCheckBox RenderCurrentLandblock;
        private HudCheckBox UpdateViewTransform;

        private Renderer Renderer = null;
        private VideoPatch VideoPatch = null;

        private static Guid IID_IDirect3DDevice9 = new Guid("{D0223B96-BF7A-43fd-92BD-A43B0D82B9EB}");
        private IntPtr unmanagedD3dPtr;
        private Device d3Ddevice;
        private bool hasD3DLayout;

        public static Dictionary<string, bool> AllowedOverrides = new Dictionary<string, bool>();
        public static Dictionary<string, object> OverrideValues = new Dictionary<string, object>();
        public static List<PropertyInfo> TrackedProps = new List<PropertyInfo>();
        public static Dictionary<PropertyInfo, HudControl> TrackedPropControls = new Dictionary<PropertyInfo, HudControl>();

        [System.Xml.Serialization.XmlIgnoreAttribute]
        public CellDatDatabase CellDat { get; set; }

        [System.Xml.Serialization.XmlIgnoreAttribute]
        public CellDatDatabase PortalDat { get; set; }

        #region Startup / Shutdown
        /// <summary>
        /// Called once when the plugin is loaded
        /// </summary>
        public void Startup(NetServiceHost host, CoreManager core, string pluginAssemblyDirectory, string accountName, string characterName, string serverName) {
            WriteLog($"Plugin.Startup");
            PluginAssemblyDirectory = pluginAssemblyDirectory;

            object a = CoreManager.Current.Decal.Underlying.GetD3DDevice(ref IID_IDirect3DDevice9);
            Marshal.QueryInterface(Marshal.GetIUnknownForObject(a), ref IID_IDirect3DDevice9, out unmanagedD3dPtr);
            d3Ddevice = new Device(unmanagedD3dPtr);

            CreateView();
            LoadDats();

            EnableRendererCheckbox_Change(null, null);
            EnableVideoPatchCheckbox_Change(null, null);

            CoreManager.Current.RenderFrame += Current_RenderFrame;
        }

        private void LoadDats() {
            try {
                var datDirectory = @"C:\Turbine\Asheron's Call\";

                var cellDatPath = Path.Combine(datDirectory, "client_cell_1.dat");
                var portalDatPath = Path.Combine(datDirectory, "client_portal.dat");

                if (!File.Exists(cellDatPath)) {
                    CoreManager.Current.Actions.AddChatText($"Unable to find cell dat: {cellDatPath}", 1);
                    return;
                }

                if (!File.Exists(portalDatPath)) {
                    CoreManager.Current.Actions.AddChatText($"Unable to find portal dat: {portalDatPath}", 1);
                    return;
                }

                CellDat = new CellDatDatabase(cellDatPath, true);
                PortalDat = new CellDatDatabase(portalDatPath, true);
            }
            catch (Exception ex) { LogException(ex); }
        }

        private void Current_RenderFrame(object sender, EventArgs e) {
            try {
                if (CoreManager.Current.CharacterFilter == null || CoreManager.Current.CharacterFilter.LoginStatus != 3)
                    return;

                if (ShouldEnableRenderer) {
                    Renderer.Render();
                }
            }
            catch (Exception ex) { LogException(ex); }
        }

        internal void UpdateRenderStateViewOptions() {
            if (view.Visible) {
                foreach (var prop in TrackedProps) {
                    if (!(AllowedOverrides.TryGetValue(prop.Name, out bool value) && value) && TrackedPropControls.TryGetValue(prop, out HudControl control)) {
                        if (prop.PropertyType == typeof(bool)) {
                            ((HudCheckBox)control).Checked = (bool)prop.GetValue(d3Ddevice.RenderState, null);
                        }
                        else if (prop.PropertyType.IsEnum) {
                            var combo = (HudCombo)control;
                            var current = prop.GetValue(d3Ddevice.RenderState, null).ToString();
                            for (var i = 0; i < combo.Count; i++) {
                                if (((HudStaticText)combo[i]).Text.Equals(current)) {
                                    combo.Current = i;
                                    break;
                                }
                            }
                        }
                        else if (prop.PropertyType == typeof(int) && !prop.Name.Contains("Color")) {
                            ((HudTextBox)control).Text = prop.GetValue(d3Ddevice.RenderState, null).ToString();
                        }
                        else if (prop.PropertyType == typeof(float)) {
                            ((HudTextBox)control).Text = prop.GetValue(d3Ddevice.RenderState, null).ToString();
                        }
                        else if ((prop.PropertyType == typeof(int) && prop.Name.Contains("Color")) || prop.PropertyType == typeof(Color)) {
                            var imageStack = (HudImageStack)control;
                            imageStack.Clear();

                            var imageRect = new Rectangle(0, 0, 50, 16);
                            if (prop.PropertyType == typeof(int)) {
                                imageStack.Add(imageRect, new ACImage(Color.FromArgb((int)prop.GetValue(d3Ddevice.RenderState, null))));
                            }
                            else if (prop.PropertyType == typeof(Color)) {
                                imageStack.Add(imageRect, new ACImage((Color)prop.GetValue(d3Ddevice.RenderState, null)));
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Called when the plugin is shutting down.  Unregister from any events here and do any cleanup.
        /// </summary>
        public void Shutdown() {
            DisableRenderer();
            DisableVideoPatch();

            CoreManager.Current.RenderFrame -= Current_RenderFrame;
            EnableRendererCheckbox.Change -= EnableRendererCheckbox_Change;
            EnableVideoPatchCheckbox.Change -= EnableVideoPatchCheckbox_Change;
            Notebook.OpenTabChange -= Notebook_OpenTabChange;

            view.Visible = false;
            view.Dispose();
        }
        #endregion

        #region VVS Views
        /// <summary>
        /// Create our VVS view from an xml template.  We also assign references to the ui elements, as well 
        /// as register event handlers.
        /// </summary>
        private void CreateView() {
            new Decal3XMLParser().ParseFromResource("ACRenderTestPlugin.Views.MainView.xml", out properties, out controls);

            // main plugin view
            view = new HudView(properties, controls);

            // ui element references
            // These name indexes in view come from the viewxml from above 
            EnableRendererCheckbox = (HudCheckBox)view["EnableRenderer"];
            EnableVideoPatchCheckbox = (HudCheckBox)view["EnableVideoPatch"];
            D3DBooleanOptionsLayout = (HudFixedLayout)view["D3DBooleanOptionsLayout"];
            D3DEnumOptionsLayout = (HudFixedLayout)view["D3DEnumOptionsLayout"];
            D3DEnum2OptionsLayout = (HudFixedLayout)view["D3DEnum2OptionsLayout"];
            D3DIntOptionsLayout = (HudFixedLayout)view["D3DIntOptionsLayout"];
            D3DFloatOptionsLayout = (HudFixedLayout)view["D3DFloatOptionsLayout"];
            D3DColorOptionsLayout = (HudFixedLayout)view["D3DColorOptionsLayout"];
            Notebook = (HudTabView)view["Notebook"];
            D3DNotebook = (HudTabView)view["D3DNotebook"];
            RenderCurrentLandblock = (HudCheckBox)view["RenderCurrentLandblock"];
            UpdateViewTransform = (HudCheckBox)view["UpdateViewTransform"];

            // ui event handlers
            EnableRendererCheckbox.Change += EnableRendererCheckbox_Change;
            EnableVideoPatchCheckbox.Change += EnableVideoPatchCheckbox_Change;
            Notebook.OpenTabChange += Notebook_OpenTabChange;
            D3DNotebook.OpenTabChange += D3DNotebook_OpenTabChange;
            RenderCurrentLandblock.Change += RenderCurrentLandblock_Change;
            UpdateViewTransform.Change += UpdateViewTransform_Change;

            EnableRendererCheckbox.Checked = ShouldEnableRenderer;
            EnableVideoPatchCheckbox.Checked = ShouldEnableVideoPatch;
            RenderCurrentLandblock.Checked = ShouldRenderCurrentLandblock;
            UpdateViewTransform.Checked = ShouldUpdateViewTransform;

            Notebook_OpenTabChange(null, null);

            BuildD3DLayout();
        }

        private void UpdateViewTransform_Change(object sender, EventArgs e) {
            ShouldUpdateViewTransform = UpdateViewTransform.Checked;
        }

        private void RenderCurrentLandblock_Change(object sender, EventArgs e) {
            ShouldRenderCurrentLandblock = RenderCurrentLandblock.Checked;
        }

        private void Notebook_OpenTabChange(object sender, EventArgs e) {
            switch (Notebook.CurrentTab) {
                case 0:
                    view.Height = 160;
                    break;
                case 1:
                    D3DNotebook_OpenTabChange(null, null);
                    break;
            }
        }

        private void D3DNotebook_OpenTabChange(object sender, EventArgs e) {
            if (Notebook.CurrentTab != 1)
                return;
            switch (D3DNotebook.CurrentTab) {
                case 0:
                    view.Height = 465;
                    break;
                case 1:
                    view.Height = 630;
                    break;
                case 2:
                    view.Height = 320;
                    break;
                case 3:
                    view.Height = 140;
                    break;
                case 4:
                    view.Height = 360;
                    break;
            }
        }

        private void EnableVideoPatchCheckbox_Change(object sender, EventArgs e) {
            if (EnableVideoPatchCheckbox.Checked) {
                DisableVideoPatch();
                ShouldEnableVideoPatch = true;
                VideoPatch = new VideoPatch();
            }
            else {
                DisableVideoPatch();
                ShouldEnableVideoPatch = false;
            }
        }

        private void EnableRendererCheckbox_Change(object sender, EventArgs e) {
            if (EnableRendererCheckbox.Checked) {
                DisableRenderer();
                ShouldEnableRenderer = true;
                Renderer = new Renderer(this, d3Ddevice, CellDat, PortalDat);
            }
            else {
                DisableRenderer();
                ShouldEnableRenderer = false;
            }
        }

        private void BuildD3DLayout() {
            if (hasD3DLayout)
                return;
            try {
                TrackedProps = typeof(RenderStateManager).GetProperties().ToList();
                TrackedProps.Sort((a, b) => a.Name.CompareTo(b.Name));

                var boolYOffset = 0;
                var enumYOffset = 0;
                var enum2YOffset = 0;
                var intYOffset = 0;
                var floatYOffset = 0;
                var colorYOffset = 0;

                foreach (var prop in TrackedProps) {
                    var height = 16;
                    var trackedProp = prop;

                    var overrideCheckbox = new HudCheckBox();
                    var overrideWidth = 70;
                    overrideCheckbox.Text = "Override";

                    if (prop.PropertyType == typeof(bool)) {
                        var checkbox = new HudCheckBox();
                        TrackedPropControls.Add(trackedProp, checkbox);
                        checkbox.Checked = (bool)trackedProp.GetValue(d3Ddevice.RenderState, null);
                        checkbox.Text = trackedProp.Name;
                        checkbox.Change += (s, e) => {
                            if (AllowedOverrides.TryGetValue(trackedProp.Name, out bool canOverride) && canOverride) {
                                if (OverrideValues.ContainsKey(trackedProp.Name)) {
                                    OverrideValues[trackedProp.Name] = checkbox.Checked;
                                }
                                else {
                                    OverrideValues.Add(trackedProp.Name, checkbox.Checked);
                                }

                                WriteLog($"Changed {trackedProp.Name} ({trackedProp.PropertyType}) to {checkbox.Checked}");
                            }
                            else {
                                checkbox.Checked = (bool)trackedProp.GetValue(d3Ddevice.RenderState, null);
                            }
                        };

                        overrideCheckbox.Change += (s, e) => {
                            if (AllowedOverrides.ContainsKey(trackedProp.Name)) {
                                AllowedOverrides[trackedProp.Name] = overrideCheckbox.Checked;
                            }
                            else {
                                AllowedOverrides.Add(trackedProp.Name, overrideCheckbox.Checked);
                            }
                        };

                        D3DBooleanOptionsLayout.AddControl(overrideCheckbox, new Rectangle(0, boolYOffset, overrideWidth, height));
                        D3DBooleanOptionsLayout.AddControl(checkbox, new Rectangle(overrideWidth, boolYOffset, D3DBooleanOptionsLayout.ClipRegion.Width - overrideWidth, height));

                        boolYOffset += height;
                    }
                    else if (prop.PropertyType.IsEnum) {
                        height = 18;
                        var comboWidth = 100;
                        var combo = new HudCombo(new ControlGroup());
                        TrackedPropControls.Add(trackedProp, combo);
                        var label = new HudStaticText();
                        label.Text = prop.Name;

                        foreach (var v in Enum.GetValues(prop.PropertyType)) {
                            var t = v.ToString();
                            combo.AddItem(t, t);
                            if (t.Equals(trackedProp.GetValue(d3Ddevice.RenderState, null).ToString())) {
                                combo.Current = combo.Count - 1;
                            }
                        }

                        if (trackedProp.Name.StartsWith("Wrap")) {
                            D3DEnum2OptionsLayout.AddControl(overrideCheckbox, new Rectangle(0, enum2YOffset, overrideWidth, height));
                            D3DEnum2OptionsLayout.AddControl(combo, new Rectangle(overrideWidth, enum2YOffset, comboWidth, height));
                            D3DEnum2OptionsLayout.AddControl(label, new Rectangle(overrideWidth + comboWidth + 10, enum2YOffset, D3DBooleanOptionsLayout.ClipRegion.Width - overrideWidth - comboWidth - 10, height));

                            enum2YOffset += height;
                        }
                        else {
                            D3DEnumOptionsLayout.AddControl(overrideCheckbox, new Rectangle(0, enumYOffset, overrideWidth, height));
                            D3DEnumOptionsLayout.AddControl(combo, new Rectangle(overrideWidth, enumYOffset, comboWidth, height));
                            D3DEnumOptionsLayout.AddControl(label, new Rectangle(overrideWidth + comboWidth + 10, enumYOffset, D3DBooleanOptionsLayout.ClipRegion.Width - overrideWidth - comboWidth - 10, height));

                            enumYOffset += height;
                        }

                        combo.Change += (s, e) => {
                            var value = Enum.Parse(trackedProp.PropertyType, ((HudStaticText)combo[combo.Current]).Text);

                            if (AllowedOverrides.TryGetValue(trackedProp.Name, out bool canOverride) && canOverride) {
                                if (OverrideValues.ContainsKey(trackedProp.Name)) {
                                    OverrideValues[trackedProp.Name] = value;
                                }
                                else {
                                    OverrideValues.Add(trackedProp.Name, value);
                                }

                                WriteLog($"Changed {trackedProp.Name} ({trackedProp.PropertyType}) to {value}");
                            }
                            else {
                                for (var i = 0; i < combo.Count; i++) {
                                    var t = ((HudStaticText)combo[i]).Text;
                                    if (t.Equals(trackedProp.GetValue(d3Ddevice.RenderState, null).ToString())) {
                                        combo.Current = combo.Count - 1;
                                    }
                                }
                            }
                        };

                        overrideCheckbox.Change += (s, e) => {
                            if (AllowedOverrides.ContainsKey(trackedProp.Name)) {
                                AllowedOverrides[trackedProp.Name] = overrideCheckbox.Checked;
                            }
                            else {
                                AllowedOverrides.Add(trackedProp.Name, overrideCheckbox.Checked);
                            }
                        };
                    }
                    else if (prop.PropertyType == typeof(int) && !prop.Name.Contains("Color")) {
                        height = 18;
                        var editWidth = 100;
                        var edit = new HudTextBox();
                        TrackedPropControls.Add(trackedProp, edit);
                        var label = new HudStaticText();
                        label.Text = prop.Name;
                        edit.Text = trackedProp.GetValue(d3Ddevice.RenderState, null).ToString();

                        D3DIntOptionsLayout.AddControl(overrideCheckbox, new Rectangle(0, intYOffset, overrideWidth, height));
                        D3DIntOptionsLayout.AddControl(edit, new Rectangle(overrideWidth, intYOffset, editWidth, height));
                        D3DIntOptionsLayout.AddControl(label, new Rectangle(overrideWidth + editWidth + 10, intYOffset, D3DBooleanOptionsLayout.ClipRegion.Width - overrideWidth - editWidth - 10, height));

                        intYOffset += height;

                        edit.Change += (s, e) => {
                            int value = 0;
                            if (!Int32.TryParse(edit.Text, out value)) {
                                CoreManager.Current.Actions.AddChatText($"Unable to parse int: {edit.Text}", 1);
                                return;
                            }

                            if (AllowedOverrides.TryGetValue(trackedProp.Name, out bool canOverride) && canOverride) {
                                if (OverrideValues.ContainsKey(trackedProp.Name)) {
                                    OverrideValues[trackedProp.Name] = value;
                                }
                                else {
                                    OverrideValues.Add(trackedProp.Name, value);
                                }

                                WriteLog($"Changed {trackedProp.Name} ({trackedProp.PropertyType}) to {value}");
                            }
                            else {
                                edit.Text = trackedProp.GetValue(d3Ddevice.RenderState, null).ToString();
                            }
                        };

                        overrideCheckbox.Change += (s, e) => {
                            if (AllowedOverrides.ContainsKey(trackedProp.Name)) {
                                AllowedOverrides[trackedProp.Name] = overrideCheckbox.Checked;
                            }
                            else {
                                AllowedOverrides.Add(trackedProp.Name, overrideCheckbox.Checked);
                            }
                        };
                    }
                    else if (prop.PropertyType == typeof(float)) {
                        height = 18;
                        var editWidth = 100;
                        var edit = new HudTextBox();
                        TrackedPropControls.Add(trackedProp, edit);
                        var label = new HudStaticText();
                        label.Text = prop.Name;
                        edit.Text = trackedProp.GetValue(d3Ddevice.RenderState, null).ToString();

                        D3DFloatOptionsLayout.AddControl(overrideCheckbox, new Rectangle(0, floatYOffset, overrideWidth, height));
                        D3DFloatOptionsLayout.AddControl(edit, new Rectangle(overrideWidth, floatYOffset, editWidth, height));
                        D3DFloatOptionsLayout.AddControl(label, new Rectangle(overrideWidth + editWidth + 10, floatYOffset, D3DBooleanOptionsLayout.ClipRegion.Width - overrideWidth - editWidth - 10, height));

                        floatYOffset += height;

                        edit.Change += (s, e) => {
                            float value = 0;
                            if (!float.TryParse(edit.Text, out value)) {
                                CoreManager.Current.Actions.AddChatText($"Unable to parse float: {edit.Text}", 1);
                                return;
                            }

                            if (AllowedOverrides.TryGetValue(trackedProp.Name, out bool canOverride) && canOverride) {
                                if (OverrideValues.ContainsKey(trackedProp.Name)) {
                                    OverrideValues[trackedProp.Name] = value;
                                }
                                else {
                                    OverrideValues.Add(trackedProp.Name, value);
                                }

                                WriteLog($"Changed {trackedProp.Name} ({trackedProp.PropertyType}) to {value}");
                            }
                            else {
                                edit.Text = trackedProp.GetValue(d3Ddevice.RenderState, null).ToString();
                            }
                        };

                        overrideCheckbox.Change += (s, e) => {
                            if (AllowedOverrides.ContainsKey(trackedProp.Name)) {
                                AllowedOverrides[trackedProp.Name] = overrideCheckbox.Checked;
                            }
                            else {
                                AllowedOverrides.Add(trackedProp.Name, overrideCheckbox.Checked);
                            }
                        };
                    }
                    else if ((prop.PropertyType == typeof(int) && prop.Name.Contains("Color")) || prop.PropertyType == typeof(Color)) {
                        height = 24;
                        var editWidth = 50;
                        var imageStack = new HudImageStack();
                        TrackedPropControls.Add(trackedProp, imageStack);
                        var label = new HudStaticText();
                        label.Text = prop.Name;

                        var imageRect = new Rectangle(0, 0, 50, 16);

                        if (prop.PropertyType == typeof(int)) {
                            imageStack.Add(imageRect, new ACImage(Color.FromArgb((int)trackedProp.GetValue(d3Ddevice.RenderState, null))));
                        }
                        else if (prop.PropertyType == typeof(Color)) {
                            imageStack.Add(imageRect, new ACImage((Color)trackedProp.GetValue(d3Ddevice.RenderState, null)));
                        }

                        D3DColorOptionsLayout.AddControl(overrideCheckbox, new Rectangle(0, colorYOffset, overrideWidth, height));
                        D3DColorOptionsLayout.AddControl(imageStack, new Rectangle(overrideWidth, colorYOffset + 2, editWidth, height - 2));
                        D3DColorOptionsLayout.AddControl(label, new Rectangle(overrideWidth + editWidth + 10, colorYOffset, D3DBooleanOptionsLayout.ClipRegion.Width - overrideWidth - editWidth - 10, height));

                        colorYOffset += height;

                        imageStack.Hit += (s, e) => {
                            if (!AllowedOverrides.TryGetValue(trackedProp.Name, out bool canOverride) || !canOverride) {
                                WriteLog($"Enable override for this color if you want to change it.");
                                return;
                            }

                            Color defaultColor = Color.White;

                            if (trackedProp.PropertyType == typeof(int)) {
                                defaultColor = Color.FromArgb((int)trackedProp.GetValue(d3Ddevice.RenderState, null));
                            }
                            else if (trackedProp.PropertyType == typeof(Color)) {
                                defaultColor = (Color)(trackedProp.GetValue(d3Ddevice.RenderState, null));
                            }

                            var picker = new ColorPicker(trackedProp.Name, defaultColor);
                            picker.RaiseColorPickerChangeEvent += (s2, e2) => {
                                SetPropColor(trackedProp, e2.Color, imageStack);
                            };

                            picker.RaiseColorPickerCancelEvent += (s2, e2) => {
                                SetPropColor(trackedProp, defaultColor, imageStack);
                                picker.Dispose();
                            };

                            picker.RaiseColorPickerSaveEvent += (s2, e2) => {
                                SetPropColor(trackedProp, e2.Color, imageStack);

                                WriteLog($"Changed {trackedProp.Name} ({trackedProp.PropertyType}) to {e2.Color}");
                                picker.Dispose();
                            };
                        };

                        overrideCheckbox.Change += (s, e) => {
                            if (AllowedOverrides.ContainsKey(trackedProp.Name)) {
                                AllowedOverrides[trackedProp.Name] = overrideCheckbox.Checked;
                            }
                            else {
                                AllowedOverrides.Add(trackedProp.Name, overrideCheckbox.Checked);
                            }
                        };
                    }
                    else {
                        WriteLog($" Found Unhandled D3D Prop {prop.Name} of type {prop.PropertyType}"); 
                    }
                }
                hasD3DLayout = true;
            }
            catch (Exception ex) { LogException(ex); }
        }

        private void SetPropColor(PropertyInfo trackedProp, Color color, HudImageStack imageStack) {
            object value = null;
            if (trackedProp.PropertyType == typeof(int)) {
                value = color.ToArgb();
            }
            else if (trackedProp.PropertyType == typeof(Color)) {
                value = color;
            }

            if (AllowedOverrides.TryGetValue(trackedProp.Name, out bool canOverride) && canOverride) {
                if (OverrideValues.ContainsKey(trackedProp.Name)) {
                    OverrideValues[trackedProp.Name] = value;
                }
                else {
                    OverrideValues.Add(trackedProp.Name, value);
                }
            }
        }

        public static ACImage GetColorIcon(int v) {
            var colorPreviewBitmap = new Bitmap(32, 32);

            using (Graphics gfx = Graphics.FromImage(colorPreviewBitmap)) {
                using (SolidBrush brush = new SolidBrush(Color.FromArgb(v))) {
                    gfx.FillRectangle(brush, 0, 0, 32, 32);
                }
            }

            return new ACImage(colorPreviewBitmap);
        }
        #endregion

        private void DisableRenderer() {
            if (Renderer != null) {
                Renderer.Dispose();
                Renderer = null;
            }
        }
        private void DisableVideoPatch() {
            if (VideoPatch != null) {
                VideoPatch.Dispose();
                VideoPatch = null;
            }
        }

        #region Logging
        public static void WriteLog(string message) {
            try {
                using (StreamWriter writer = new StreamWriter(Path.Combine(PluginAssemblyDirectory, "exceptions.txt"), true)) {
                    writer.WriteLine(message);
                    CoreManager.Current.Actions.AddChatText($"ACRenderTest: {message}", 1);
                    writer.Close();
                }
            }
            catch { }
        }
        public static void LogException(Exception ex) {
            try {
                using (StreamWriter writer = new StreamWriter(Path.Combine(PluginAssemblyDirectory, "exceptions.txt"), true)) {
                    writer.WriteLine(ex.ToString());
                    CoreManager.Current.Actions.AddChatText($"ACRenderTest Error: {ex}", 5);
                    writer.Close();
                }
            }
            catch { }
        }
        #endregion
    }
}
