﻿using ACE.DatLoader;
using ACE.DatLoader.Entity;
using ACE.DatLoader.FileTypes;
using Decal.Adapter;
using Microsoft.DirectX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACRenderTestPlugin {
    class DungeonGeometry {

        private PluginLogic PL;

        private CellDatDatabase CellDat;
        private CellDatDatabase PortalDat;

        public enum GeometryType {
            Unknown,
            Floor,
            StaticObject
        }

        public List<Vector3> Vertices { get; } = new List<Vector3>();
        public Dictionary<int, GeometryType> VerticesTypes = new Dictionary<int, GeometryType>();
        public List<List<int>> WalkableTriangles { get; } = new List<List<int>>();
        public List<List<int>> StaticTriangles { get; } = new List<List<int>>();
        public List<List<int>> OtherTriangles { get; } = new List<List<int>>();
        public uint Landblock { get; }

        public DungeonGeometry(PluginLogic plugin, CellDatDatabase cellDat, CellDatDatabase portalDat, uint landblock) {
            CellDat = cellDat;
            PortalDat = portalDat;
            PL = plugin;
            Landblock = landblock;

            LoadLandblock();
        }

        private void LoadLandblock() {
            try {
                WalkableTriangles.Clear();
                OtherTriangles.Clear();
                StaticTriangles.Clear();
                Vertices.Clear();
                VerticesTypes.Clear();

                var lbInfoId = (uint)(0xFFFE + (Landblock & 0xFFFF0000));
                CoreManager.Current.Actions.AddChatText($"Attempting to load landblock: 0x{lbInfoId:X8}", 1);

                var landblockInfo = CellDat.ReadFromDat<LandblockInfo>(lbInfoId);
                var frames = new List<Frame>();

                for (var i = 0; i < landblockInfo.NumCells; i++) {
                    EnvCell cell = CellDat.ReadFromDat<EnvCell>((uint)((landblockInfo.Id >> 16 << 16) + 0x00000100 + i)); 

                    var tempFrames = frames.ToList();
                    tempFrames.Add(cell.Position);

                    LoadEnvironment(cell.EnvironmentId, tempFrames, cell.CellStructure);

                    if (cell.StaticObjects != null && cell.StaticObjects.Count > 0) {
                        foreach (var staticObject in cell.StaticObjects) {
                            var sFrames = frames.ToList();
                            sFrames.Add(staticObject.Frame);
                            LoadSetupOrGfxObj(staticObject.Id, sFrames, GeometryType.StaticObject);
                        }
                    }
                }

                if (landblockInfo.Objects != null && landblockInfo.Objects.Count > 0) {
                    for (var i = 0; i < landblockInfo.Objects.Count; i++) {
                        var obj = landblockInfo.Objects[i];
                        var sFrames = frames.ToList();
                        sFrames.Add(obj.Frame);
                        LoadSetupOrGfxObj(obj.Id, sFrames, GeometryType.StaticObject);
                    }
                }
            }
            catch (Exception ex) { PluginLogic.LogException(ex); }
        }

        private void LoadEnvironment(uint cellFileIndex, List<Frame> frames, int envCellIndex = -1) {
            var environment = PortalDat.ReadFromDat<ACE.DatLoader.FileTypes.Environment>(cellFileIndex);
            if (environment.Id == (uint)0x0D0000CA || environment.Id == (uint)0x0D00016D)
                return;

            if (envCellIndex >= 0) {
                var polys = environment.Cells[(uint)envCellIndex].PhysicsPolygons;
                foreach (var poly in polys.Values) {
                    AddPolygon(poly.VertexIds.Select(v => environment.Cells[(uint)envCellIndex].VertexArray.Vertices[(ushort)v].Origin).ToList(), frames, GeometryType.Unknown);
                }
            }
            else {
                foreach (var envCell in environment.Cells.Values) {
                    foreach (var poly in envCell.PhysicsPolygons.Values) {
                        AddPolygon(poly.VertexIds.Select(v => envCell.VertexArray.Vertices[(ushort)v].Origin).ToList(), frames, GeometryType.Unknown);
                    }
                }
            }
        }

        private void LoadSetupOrGfxObj(uint id, List<Frame> sFrames, GeometryType gType) {
            if ((id & 0x02000000) != 0) {
                LoadSetup(id, sFrames, gType);
            }
            else {
                LoadGfxObj(id, sFrames, gType);
            }
        }

        private void LoadGfxObj(uint cellFileIndex, List<Frame> frames, GeometryType gType) {
            var gfxObj = PortalDat.ReadFromDat<GfxObj>(cellFileIndex);
            if (gfxObj == null)
                return;
            foreach (var pkv in gfxObj.PhysicsPolygons) {
                var vertices = pkv.Value.VertexIds.Select(v => gfxObj.VertexArray.Vertices[(ushort)v].Origin).ToList();
                AddPolygon(vertices, frames, gType);
            }
        }

        private void LoadSetup(uint cellFileIndex, List<Frame> frames, GeometryType gType) {
            var setupModel = PortalDat.ReadFromDat<SetupModel>(cellFileIndex);

            // physics draw priority is child part physics polys, then cylspheres, then spheres?
            foreach (var partId in setupModel.Parts) {
                var gfxObj = PortalDat.ReadFromDat<GfxObj>(partId);
                if (gfxObj.PhysicsPolygons != null && gfxObj.PhysicsPolygons.Count > 0) {
                    //hasPhysicsPolys = true;
                    break;
                }
            }

            /*
            if (!hasPhysicsPolys && setupModel.CylSpheres != null && setupModel.CylSpheres.Count > 0) {
                foreach (var cSphere in setupModel.CylSpheres) {
                    List<List<Vector3>> polys = GetCylSpherePolygons(cSphere.Origin, cSphere.Height, cSphere.Radius, coneTop);
                    foreach (var poly in polys) {
                        AddPolygon(poly, frames);
                    }
                }
            }

            if (!hasPhysicsPolys && setupModel.Spheres != null && setupModel.Spheres.Count > 0) {
                foreach (var sphere in setupModel.Spheres) {
                    List<List<Vector3>> polys = GetSpherePolygons(sphere.Origin, sphere.Radius);
                    foreach (var poly in polys) {
                        AddPolygon(poly, frames);
                    }
                }
            }
            */

            // draw all the child parts
            for (var i = 0; i < setupModel.Parts.Count; i++) {
                // always use PlacementFrames[0] ?
                var tempFrames = frames.ToList(); // clone
                tempFrames.Insert(0, setupModel.PlacementFrames[0].AnimFrame.Frames[i]);
                LoadGfxObj(setupModel.Parts[i], tempFrames, gType);
            }
        }

        public void AddPolygon(List<Vector3> vertices, List<Frame> frames, GeometryType gType) {
            var poly = new List<Vector3>();
            for (var i = 0; i < vertices.Count; i++) {
                var vertice = vertices[i];
                foreach (var frame in frames) {
                    vertice = Transform(vertice, frame.Orientation);
                    vertice += frame.Origin;
                }

                vertice = new Vector3() {
                    X = vertice.X,
                    Y = vertice.Y,
                    Z = vertice.Z
                };

                poly.Add(vertice);
            }

            // split to triangles
            for (int i = 2; i < poly.Count; i++) {
                var tri = new List<int>();

                Vertices.Add(poly[i]);
                tri.Add(Vertices.Count - 1);

                Vertices.Add(poly[i - 1]);
                tri.Add(Vertices.Count - 1);

                Vertices.Add(poly[0]);
                tri.Add(Vertices.Count - 1);

                if (CanWalkTri(tri, gType)) {
                    gType = GeometryType.Floor;
                }

                VerticesTypes.Add(Vertices.Count - 1, gType);
                VerticesTypes.Add(Vertices.Count - 2, gType);
                VerticesTypes.Add(Vertices.Count - 3, gType);

                switch (gType) {
                    case GeometryType.Floor:
                        WalkableTriangles.Add(tri);
                        break;
                    case GeometryType.StaticObject:
                        StaticTriangles.Add(tri);
                        break;
                    default:
                        OtherTriangles.Add(tri);
                        break;
                }
            }
        }
        private Vector3 CalculateTriSurfaceNormal(Vector3 a, Vector3 b, Vector3 c) {
            Vector3 normal = new Vector3();
            Vector3 u = new Vector3();
            Vector3 v = new Vector3();

            u.X = b.X - a.X;
            u.Y = b.Y - a.Y;
            u.Z = b.Z - a.Z;

            v.X = c.X - a.X;
            v.Y = c.Y - a.Y;
            v.Z = c.Z - a.Z;

            normal.X = u.Y * v.Z - u.Z * v.Y;
            normal.Y = u.Z * v.X - u.X * v.Z;
            normal.Z = u.X * v.Y - u.Y * v.X;

            normal.Normalize();

            return normal;
        }

        private bool CanWalkTri(List<int> tri, GeometryType gType) {
            var floorZ = -0.66417414618662751f;
            var triNormal = CalculateTriSurfaceNormal(Vertices[tri[0]], Vertices[tri[1]], Vertices[tri[2]]);
            return triNormal.Z <= floorZ;
        }

        public static Vector3 Transform(Vector3 value, Quaternion rotation) {
            float x2 = rotation.X + rotation.X;
            float y2 = rotation.Y + rotation.Y;
            float z2 = rotation.Z + rotation.Z;

            float wx2 = rotation.W * x2;
            float wy2 = rotation.W * y2;
            float wz2 = rotation.W * z2;
            float xx2 = rotation.X * x2;
            float xy2 = rotation.X * y2;
            float xz2 = rotation.X * z2;
            float yy2 = rotation.Y * y2;
            float yz2 = rotation.Y * z2;
            float zz2 = rotation.Z * z2;

            return new Vector3(
                value.X * (1.0f - yy2 - zz2) + value.Y * (xy2 - wz2) + value.Z * (xz2 + wy2),
                value.X * (xy2 + wz2) + value.Y * (1.0f - xx2 - zz2) + value.Z * (yz2 - wx2),
                value.X * (xz2 - wy2) + value.Y * (yz2 + wx2) + value.Z * (1.0f - xx2 - yy2));
        }
    }
}
