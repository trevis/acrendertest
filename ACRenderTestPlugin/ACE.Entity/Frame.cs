using Microsoft.DirectX;

namespace ACE.Entity
{
    public class Frame
    {
        public Vector3 Origin { get; set; }
        public Quaternion Orientation { get; set; }

        public Frame()
        {
            Origin = new Vector3();
            Orientation = Quaternion.Identity;
        }
    }
}
