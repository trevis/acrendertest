using Microsoft.DirectX;

namespace ACE.Entity
{
    public class Plane
    {
        public Vector3 N { get; set; }
        public float D { get; set; }
    }
}
