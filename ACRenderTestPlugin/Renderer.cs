﻿using ACE.DatLoader;
using ACE.DatLoader.Entity;
using ACE.DatLoader.FileTypes;
using Decal.Adapter;
using Decal.Filters;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace ACRenderTestPlugin {
    public class Renderer : IDisposable {
        private Device d3Ddevice;

        private CellDatDatabase CellDat;
        private CellDatDatabase PortalDat;

        private uint LoadedLandblock;

        private PluginLogic PL;
        private DungeonGeometry DungeonGeometry;

        public int[] OtherTriIndexes { get; private set; }
        public int[] StaticTriIndexes { get; private set; }

        private IndexBuffer staticTriIndexBuffer;
        private IndexBuffer otherTriIndexBuffer;
        private IndexBuffer walkableTriIndexBuffer;
        private VertexBuffer vertexBuffer;
        private CustomVertex.PositionColored[] vertices;
        private int[] WalkableTriIndexes;

        public Renderer(PluginLogic plugin, Device d3Ddevice, CellDatDatabase cellDat, CellDatDatabase portalDat) {
            this.d3Ddevice = d3Ddevice;
            CellDat = cellDat;
            PortalDat = portalDat;
            PL = plugin;
        }

        internal void Render() {
            using (var stateBlock = new StateBlock(d3Ddevice, StateBlockType.All)) {
                stateBlock.Capture();

                if (PL.ShouldUpdateViewTransform) {
                    d3Ddevice.Transform.View = Camera.GetD3DViewTransform();
                }

                d3Ddevice.Transform.World = Matrix.Identity;

                d3Ddevice.RenderState.CullMode = Cull.CounterClockwise;
                d3Ddevice.RenderState.Lighting = false;

                d3Ddevice.RenderState.ZBufferEnable = true;
                d3Ddevice.RenderState.ZBufferWriteEnable = true;
                d3Ddevice.RenderState.ZBufferFunction = Compare.LessEqual;

                d3Ddevice.RenderState.DepthBias = 1;
                d3Ddevice.RenderState.FogEnable = true;
                d3Ddevice.RenderState.FogStart = 0;
                d3Ddevice.RenderState.FogEnd = 90;
                d3Ddevice.RenderState.FogColor = Color.FromArgb(255, 0, 20, 20);

                d3Ddevice.SetTexture(0, null);

                // use RenderState overrides from the ui
                foreach (var kv in PluginLogic.AllowedOverrides) {
                    if (kv.Value == true && PluginLogic.OverrideValues.TryGetValue(kv.Key, out object value)) {
                        d3Ddevice.RenderState.GetType().GetProperty(kv.Key).SetValue(d3Ddevice.RenderState, value, null);
                    }
                }

                if (PL.ShouldRenderCurrentLandblock) {
                    RenderCurrentLandblock();
                }

                PL.UpdateRenderStateViewOptions();

                stateBlock.Apply();
            }

        }

        private void RenderCurrentLandblock() {
            CheckLandblock();

            d3Ddevice.VertexFormat = CustomVertex.PositionColored.Format;
            d3Ddevice.SetStreamSource(0, vertexBuffer, 0);

            if (OtherTriIndexes.Length > 0) {
                d3Ddevice.RenderState.CullMode = Cull.CounterClockwise;
                d3Ddevice.Indices = otherTriIndexBuffer;
                d3Ddevice.RenderState.FillMode = FillMode.WireFrame;
                d3Ddevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, OtherTriIndexes.Length, 0, OtherTriIndexes.Length / 3);
                d3Ddevice.RenderState.FillMode = FillMode.Solid;
                d3Ddevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, OtherTriIndexes.Length, 0, OtherTriIndexes.Length / 3);
            }

            if (WalkableTriIndexes.Length > 0) {
                d3Ddevice.RenderState.CullMode = Cull.None;
                d3Ddevice.Indices = walkableTriIndexBuffer;
                d3Ddevice.RenderState.FillMode = FillMode.WireFrame;
                d3Ddevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, WalkableTriIndexes.Length, 0, WalkableTriIndexes.Length / 3);
                d3Ddevice.RenderState.FillMode = FillMode.Solid;
                d3Ddevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, WalkableTriIndexes.Length, 0, WalkableTriIndexes.Length / 3);
            }

            if (StaticTriIndexes.Length > 0) {
                d3Ddevice.RenderState.CullMode = Cull.CounterClockwise;
                d3Ddevice.Indices = staticTriIndexBuffer;
                d3Ddevice.RenderState.FillMode = FillMode.WireFrame;
                d3Ddevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, StaticTriIndexes.Length, 0, StaticTriIndexes.Length / 3);
                d3Ddevice.RenderState.FillMode = FillMode.Solid;
                d3Ddevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, StaticTriIndexes.Length, 0, StaticTriIndexes.Length / 3);
            }
        }

        private void CheckLandblock() {
            var currentLb = (uint)(CoreManager.Current.Actions.Landcell & 0xFFFF0000);
            if (currentLb != LoadedLandblock) {
                DungeonGeometry = new DungeonGeometry(PL, CellDat, PortalDat, currentLb);
                LoadedLandblock = currentLb;
                MakeBuffers();
            }
        }

        private void MakeBuffers() {
            if (walkableTriIndexBuffer != null)
                walkableTriIndexBuffer.Dispose();
            if (otherTriIndexBuffer != null)
                otherTriIndexBuffer.Dispose();
            if (staticTriIndexBuffer != null)
                otherTriIndexBuffer.Dispose();
            if (vertexBuffer != null)
                vertexBuffer.Dispose();

            vertices = new CustomVertex.PositionColored[DungeonGeometry.Vertices.Count];
            var rand = new Random();
            int color;
            for (var i = 0; i < DungeonGeometry.Vertices.Count; i++) {
                switch (DungeonGeometry.VerticesTypes[i]) {
                    case DungeonGeometry.GeometryType.Floor:
                        var m = rand.Next(235, 255);
                        color = Color.FromArgb(200, m, m, m).ToArgb();
                        break;
                    case DungeonGeometry.GeometryType.StaticObject:
                        var v = rand.Next(235, 255);
                        color = Color.FromArgb(30, v, 0, v).ToArgb();
                        break;
                    default:
                        color = Color.FromArgb(120, 0, rand.Next(215, 255), 0).ToArgb();
                        break;
                }

                vertices[i] = new CustomVertex.PositionColored(new Vector3(
                    DungeonGeometry.Vertices[i].X,
                    DungeonGeometry.Vertices[i].Z,
                    DungeonGeometry.Vertices[i].Y
                ), color);
            }

            vertexBuffer = new VertexBuffer(vertices[0].GetType(), vertices.Length, d3Ddevice, Usage.WriteOnly, CustomVertex.PositionColored.Format, Pool.Managed);
            vertexBuffer.SetData(vertices, 0, LockFlags.None);

            var walkableTriIndexes = new List<int>();
            foreach (var triIndexes in DungeonGeometry.WalkableTriangles) {
                walkableTriIndexes.AddRange(triIndexes);
            }

            this.WalkableTriIndexes = walkableTriIndexes.ToArray();
            if (WalkableTriIndexes.Length > 0) {
                walkableTriIndexBuffer = new IndexBuffer(walkableTriIndexes[0].GetType(), walkableTriIndexes.Count, d3Ddevice, Usage.None, Pool.Managed);
                walkableTriIndexBuffer.SetData(this.WalkableTriIndexes, 0, LockFlags.None);
            }


            var otherTriIndexes = new List<int>();
            foreach (var triIndexes in DungeonGeometry.OtherTriangles) {
                otherTriIndexes.AddRange(triIndexes);
            }

            this.OtherTriIndexes = otherTriIndexes.ToArray();
            if (OtherTriIndexes.Length > 0) {
                otherTriIndexBuffer = new IndexBuffer(otherTriIndexes[0].GetType(), otherTriIndexes.Count, d3Ddevice, Usage.None, Pool.Managed);
                otherTriIndexBuffer.SetData(this.OtherTriIndexes, 0, LockFlags.None);
            }

            var staticTriIndexes = new List<int>();
            foreach (var triIndexes in DungeonGeometry.StaticTriangles) {
                staticTriIndexes.AddRange(triIndexes);
            }

            this.StaticTriIndexes = staticTriIndexes.ToArray();
            if (StaticTriIndexes.Length > 0) {
                staticTriIndexBuffer = new IndexBuffer(staticTriIndexes[0].GetType(), staticTriIndexes.Count, d3Ddevice, Usage.None, Pool.Managed);
                staticTriIndexBuffer.SetData(this.StaticTriIndexes, 0, LockFlags.None);
            }
        }

        public void Dispose() {
            if (walkableTriIndexBuffer != null)
                walkableTriIndexBuffer.Dispose();
            if (staticTriIndexBuffer != null)
                staticTriIndexBuffer.Dispose();
            if (otherTriIndexBuffer != null)
                otherTriIndexBuffer.Dispose();
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
        }
    }
}
