# ACRenderTest

AC Render Test. For messing around with d3d in game.

## What it does

- renders current dungeon environment in 3d (not outside terrain currently)
- provides ingame ui for editing all of d3d RenderState in realtime
- has video patch to disable game 3d rendering (with optional camera frame updating so you can still move around with a custom 3d view)

## Installing

- Compile
- Add ACRenderTest.dll *not ACRenderTestPlugin.dll* to decal.

## Editing

- Most of what you would want to edit is inside `ACRenderTestPlugin/DungeonGeometry.cs` and `ACRenderTestPlugin/Renderer.cs`
- Has hot reload so recompile ACRenderTestPlugin while ingame and it will automatically reload itself with changes.

## Known Issues

- doesn't currently render terrain.
- alpha blending is *kind of* broken, but good enough imo


## Screenshots

![image](/uploads/d71741b358369871b356281d34d6fa92/image.png)
![image](/uploads/2f1a1cfa40bcc7de539c11146a0f2fca/image.png)
![image](/uploads/3bfe1aaf1643533d52d38daa49d9630b/image.png)
![image](/uploads/77a65fc3a3a1866cb8150b5e2ffbcf62/image.png)
![image](/uploads/08306bbe53792fa965db48349afee96c/image.png)
![image](/uploads/e0ef55648a3c47137cc816e90fe5aaee/image.png)
![image](/uploads/f24104069f93a273cd551f151bf794fe/image.png)
![image](/uploads/779944bba437f7ea87dddfaaf2414f51/image.png)
